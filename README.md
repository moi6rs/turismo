## Proyecto base Circuito de Turismo

### 1. Requisitos
+ Angular 6.x
+ node.js
+ Angular CLI

### 2. Crear proyecto
```shell
ng new my-proyecto-angular
```
### 3. Crear Componentes, Servicios, Clases
```shell
ng g c components/home
ng g s services/home
ng g cl models/home
ng g module app-routing
```
-------------------------
## 4. Agregar en styles.css
```css
@import "~bootstrap/dist/css/bootstrap.css";
@import "~font-awesome/css/font-awesome.css";
```
### 5. Agregar a angular.json
```json
"styles": [
              "node_modules/bootstrap/dist/css/bootstrap.css",
              "node_modules/bootstrap/dist/css/bootstrap.css",
              "src/styles.css"
            ],
```
### 5. urls de ayuda 
+ [Crear un proyecto con angular y boostrap](https://justcodeit.io/arrancar-proyecto-angular-6-bootstrap-4/)
+ [Crear un proyecto angular](https://medium.com/@beeman/tutorial-styling-angular-cli-v6-apps-with-bootstrap-8d4f8ea5adae)
import {Routes} from "@angular/router";
import {CircuitoComponent} from "../components/circuito/circuito.component";
import {HomeComponent} from "../components/home/home.component";

export const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'circuitos', component: CircuitoComponent},
  {path: '**', component: HomeComponent}
];
